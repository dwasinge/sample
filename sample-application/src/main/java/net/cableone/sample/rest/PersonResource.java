package net.cableone.sample.rest;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.cableone.sample.domain.Person;
import net.cableone.sample.service.PersonService;

@RestController
@RequestMapping("/api/v1/person")
public class PersonResource {

	private PersonService sampleService;

	@Autowired
	public PersonResource(PersonService sampleService) {
		this.sampleService = sampleService;
	}

	/**
	 * Creates a new {@link Person} resource.
	 * 
	 * @param person
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Person create(@RequestBody Person person) {
		return sampleService.createPerson(person);
	}

	/**
	 * Returns the {@link Person} with the given ID. Otherwise, no content returned.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Person get(@PathVariable("id") int id) {

		Optional<Person> optional = sampleService.getPerson(id);
		return optional.isPresent() ? optional.get() : null;

	}

	/**
	 * Returns the {@link Person} deleted with the given ID. Otherwise, not content
	 * returned.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public Person delete(@PathVariable("id") int id) {

		Optional<Person> optional = sampleService.deletePerson(id);
		return optional.isPresent() ? optional.get() : null;

	}

}
