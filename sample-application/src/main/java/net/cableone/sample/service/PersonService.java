package net.cableone.sample.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Component;

import net.cableone.sample.domain.Person;

@Component
public class PersonService {

	private Map<Integer, Person> map = new HashMap<>();
	private AtomicInteger idCounter = new AtomicInteger(1);

	/**
	 * Stores the {@link Person} to the data store. This implementation just uses a
	 * {@link HashMap}.
	 * 
	 * @param person
	 * @return
	 */
	public Person createPerson(Person person) {

		// get next value
		int id = idCounter.getAndIncrement();

		// set id
		person.setId(id);

		// save person
		map.put(id, person);

		return person;

	}

	/**
	 * Returns an {@link Optional} containing the stored {@link Person} if one
	 * exists for the provided ID. Otherwise, an empty {@link Optional} is returned.
	 * 
	 * @param id
	 * @return
	 */
	public Optional<Person> getPerson(int id) {
		return null == map.get(id) ? Optional.empty() : Optional.of(map.get(id));
	}

	/**
	 * Returns an {@link Optional} containing the removed {@link Person} if one
	 * exists for the provided ID. Otherwise, an empty {@link Optional} is returned.
	 * 
	 * @param id
	 * @return
	 */
	public Optional<Person> deletePerson(int id) {

		// get value to delete if exists
		Optional<Person> optional = (null == map.get(id)) ? Optional.empty() : Optional.of(map.get(id));

		// remove value from map
		map.remove(id);

		return optional;

	}

}
