Feature:  Person API

Scenario: Person Resource is Created

Given a person with valid details
When the person is POSTed to the endpoint /person
Then person will be persisted

Scenario: Person Resource Is Not Created - Invalid Name

Given a person with invalid details
When the person is POSTed to the endpoint /person
Then person will be not persisted

